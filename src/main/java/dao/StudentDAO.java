package dao;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import pojo.Student;

public class StudentDAO {
	SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
	
	public void add(Student student) {
	    Session session = sessionFactory.openSession();
	    try {
	      session.beginTransaction();
	      session.save(student);
	      session.getTransaction().commit();
	      System.out.println("insert success!");
	    } catch (RuntimeException e) {
	      session.getTransaction().rollback();
	      e.printStackTrace();
	    } finally {
	      session.flush();
	      session.close();
	    }
	  }
	  
	  public Student findById(String id) {
	    Session session = sessionFactory.openSession();
	    Student student = session.load(Student.class, id);
	    return student;
	  }
	  //Lay danh sach tat ca hoc sinh
	  public List<Student> allStudent() {
	    Session session = sessionFactory.openSession();
	    List<Student> list = session.createQuery("FROM Student").list();
	    //for (Student student : list) {
	    //  System.out.println(student.getMSSV());
	    //}
	    return list;
	  }
	  
	  //Lay danh sach hoc sinh theo lop
	  public List<Student> allClassStudent(String Class){
		  Session session = sessionFactory.openSession();
		  List<Student> list = session.createQuery("FROM Student WHERE Lop LIKE :Lop").setParameter("Lop", "%" + Class + "%").list();		  
		  return list;
	  }
	  
	  public void updateName(int id, String name) {
		try {
			Session session = sessionFactory.openSession();
			String sql = "UPDATE Student u SET u.HoTen = :newName WHERE u.MSSV = :id";
			session.createQuery(sql).setParameter("newName", name).setParameter("id", id).executeUpdate();
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	  }
	  
	  public void update(Student student) {
	    Session session = sessionFactory.openSession();
	    session.update(student);
	  }
	  
	  public void delete(int id) {
	    Session session = sessionFactory.openSession();
	    try {
	      session.beginTransaction();
	      Student student = session.load(Student.class, id);
	      session.delete(student);
	      session.getTransaction().commit();
	      System.out.println("detete success!");
	    } catch (RuntimeException e) {
	      session.getTransaction().rollback();
	      e.printStackTrace();
	    } finally {
	      session.flush();
	      session.close();
	    }
	  }
	  public void searchByName(String name) {
	    Session session = sessionFactory.openSession();
	    List<Student> list = session.createQuery("FROM sinhvien WHERE HoTen LIKE :name").setParameter("name", "%" + name + "%").list();
	    for (Student student : list) {
	      System.out.println(student);
	    }
	  }
	
	  
}
