package dao;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import pojo.Student;
import pojo.Account;
import dao.StudentDAO;

public class AccountDAO {
	SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
	
	public void generateAccount() {
		String jdbcURL = "jdbc:mysql://localhost:3306/mydb";
        String username = "root";
        String password = "lmfao#123";
 
        int batchSize = 20;
 
        Connection connection = null;
 
        try {
 
            connection = DriverManager.getConnection(jdbcURL, username, password);
            connection.setAutoCommit(false);
 
            String sql = "INSERT INTO taikhoan (TenTaiKhoan, MatKhau, Loai) VALUES (?, ?, ?)";
            PreparedStatement statement = connection.prepareStatement(sql);
    
            int count = 0;
            StudentDAO stuDAO = new StudentDAO();
            List<Student> stuList = stuDAO.allStudent();
 
            for(Student stu : stuList) {
                String ttk = stu.getMSSV();
                String mk = stu.getMSSV();
                String loai = "1";
                           
                statement.setString(1, ttk);
                statement.setString(2, mk);
                int i = Integer.parseInt(loai);
                statement.setInt(3, i);

 
                statement.addBatch();
 
                if (count % batchSize == 0) {
                    statement.executeBatch();
                }
            }
 
            // execute the remaining queries
            statement.executeBatch();
 
            connection.commit();
            connection.close();
 
        } catch (Exception ex) {
            System.err.println(ex);
        }
	}

	public void changePassword(String username, String newpw) {
		try {
		Session session = sessionFactory.openSession();
	    String hql = "UPDATE Account set MatKhau = :mk WHERE TenTaiKhoan LIKE :ttk";
		session.createQuery(hql).setParameter("mk", "%" + newpw + "%").setParameter("ttk", "%" + username + "%").executeUpdate();
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void changePassword1(Account a) {
		Session session = sessionFactory.openSession();
	    session.update(a);
	}
}
