package dao;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import pojo.Schedule;
import java.io.*;
import java.sql.*;
import dao.StudentDAO;
import pojo.Student;

public class ScheduleDAO {
	SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
	
	public void importSchedule(String Class) {

		String jdbcURL = "jdbc:mysql://localhost:3306/mydb";
        String username = "root";
        String password = "lmfao#123";
        String csvFilePath = Class + " - TKB.csv";
 
        int batchSize = 20;
 
        Connection connection = null;
 
        try {
 
            connection = DriverManager.getConnection(jdbcURL, username, password);
            connection.setAutoCommit(false);
 
            String sql = "INSERT INTO Schedule (STT, MaMon, TenMon, PhongHoc, Lop) VALUES (?, ?, ?, ?, ?)";
            PreparedStatement statement = connection.prepareStatement(sql);
            BufferedReader lineReader = new BufferedReader(new FileReader(csvFilePath));
            String lineText = null;
 
            int count = 0;
 
            lineReader.readLine(); // skip header line
 
            while ((lineText = lineReader.readLine()) != null) {
                String[] data = lineText.split(",");
                String sTT = data[0];
                String maMon = data[1];
                String tenMon = data[2];
                String phongHoc = data[3];
                String lop = data[4];
                
                int i = Integer.parseInt(sTT);
                statement.setInt(1, i);
                statement.setString(2, maMon);
                statement.setString(3, tenMon);
                statement.setString(4, phongHoc);
                statement.setString(5, lop);

 
                statement.addBatch();
 
                if (count % batchSize == 0) {
                    statement.executeBatch();
                }
            }
 
            lineReader.close();
 
            // execute the remaining queries
            statement.executeBatch();
 
            connection.commit();
            connection.close();
 
        } catch (IOException ex) {
            System.err.println(ex);
        } catch (SQLException ex) {
            ex.printStackTrace();
 
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
	}
	
	public List<Schedule> allSchedule() {
	    Session session = sessionFactory.openSession();
	    List<Schedule> list = session.createQuery("FROM Schedule").list();
	    return list;
	 }
	
	public List<Schedule> classSchedule(String c){
		Session session = sessionFactory.openSession();
	    List<Schedule> list = session.createQuery("FROM Schedule WHERE Lop LIKE :Lop").setParameter("Lop", "%" + c + "%").list();
	    return list;
	}
		
	public void createSubjectStudentList() {	
		ScheduleDAO b = new ScheduleDAO();
		List<Schedule> d = b.allSchedule();
		for (Schedule sch : d) {
			try {
				FileWriter csvWriter = new FileWriter(sch.getLop() + "-" + sch.getMaMon() + "-DSSV.csv");
				csvWriter.append("STT");
				csvWriter.append(",");
				csvWriter.append("MSSV");
				csvWriter.append(",");
				csvWriter.append("HoTen");
				csvWriter.append(",");
				csvWriter.append("GioiTinh");
				csvWriter.append(",");
				csvWriter.append("CMND");
				csvWriter.append("\n");
				
				StudentDAO a = new StudentDAO();
				List<Student> c = a.allClassStudent(sch.getLop());
				
				for (Student rowData : c) {
				    csvWriter.append(rowData.getSTT() + "," + rowData.getMSSV() + "," + rowData.getHoTen() + "," + rowData.getGioiTinh() + "," + rowData.getCMND() + "\n");
				}

				csvWriter.flush();
				csvWriter.close();
			}catch(Exception ex) {
				ex.printStackTrace();
			}
	    }
	}
	
	public List<String> allSubjectStudent(String sub){
		ScheduleDAO scheduleDAO = new ScheduleDAO(); 
		Schedule sche = scheduleDAO.findById(sub);
		List<String> allData = new ArrayList<String>();
		try {
			//Doc file va xoa dong can tim tren buffer
			BufferedReader csvReader = new BufferedReader(new FileReader(sche.getLop() + "-" + sub + "-DSSV.csv"));
			csvReader.readLine();
			String row;
			while ((row = csvReader.readLine()) != null) {
		    	allData.add(row);		    		
		    }
			
		}catch(Exception ex) {
		    ex.printStackTrace();
		}
		return allData;
	}
	
	public void delete(String id, String mamon) {
		//Tim sinh vien co id
		StudentDAO studentDAO = new StudentDAO(); 
		Student stu = studentDAO.findById(id);
		try {
			//Doc file va xoa dong can tim tren buffer
			BufferedReader csvReader = new BufferedReader(new FileReader(stu.getLop() + "-" + mamon + "-DSSV.csv"));
			csvReader.readLine();
			String row;
			List<String> allData = new ArrayList<String>();
			while ((row = csvReader.readLine()) != null) {
		    	allData.add(row);		    		
		    }
			int index = 0;
		    for(String str : allData) {		    	
		    	String[] parts = str.split(",");
		    	if(id.equals(parts[1])) {
		    		allData.remove(index);
		    		break;
		    	}
		    	index++;
			}
		    csvReader.close();
		    
		    //Ghi lai xuong file cu
		    FileWriter csvWriter = new FileWriter(stu.getLop() + "-" + mamon + "-DSSV.csv");
			csvWriter.append("STT");
			csvWriter.append(",");
			csvWriter.append("MSSV");
			csvWriter.append(",");
			csvWriter.append("HoTen");
			csvWriter.append(",");
			csvWriter.append("GioiTinh");
			csvWriter.append(",");
			csvWriter.append("CMND");
			csvWriter.append("\n");
			
			for (String temp : allData) {
				String[] parts = temp.split(",");
			    csvWriter.append(parts[0] + "," + parts[1] + "," + parts[2] + "," + parts[3] + "," + parts[4] + "\n");
			}

			csvWriter.flush();
			csvWriter.close();
			
			//Xoa sinh vien trong bang diem
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public Schedule findById(String id) {
	    Session session = sessionFactory.openSession();
	    Schedule schedule = session.load(Schedule.class, id);
	    return schedule;
	  }

	public void add(String id, String mamon) {
		//Tim sinh vien co id
		StudentDAO studentDAO = new StudentDAO(); 
		ScheduleDAO scheduleDAO = new ScheduleDAO();
		Schedule sche = scheduleDAO.findById(mamon);
		Student stu = studentDAO.findById(id);
		
		try {
			//Doc file va xoa dong can tim tren buffer
			BufferedReader csvReader = new BufferedReader(new FileReader(sche.getLop() + "-" + mamon + "-DSSV.csv"));
			csvReader.readLine();
			String row;
			List<String> allData = new ArrayList<String>();
			while ((row = csvReader.readLine()) != null) {
		    	allData.add(row);		    		
		    }

		    for(int i=1; i <= allData.size();i++) {
		    	String temp = new String();
		    	temp = allData.get(i-1);
		    	String[] parts = temp.split(",");
		    	if(i != Integer.parseInt(parts[0])) {
		    		stu.setSTT(i);
		    		allData.add(i-1,stu.getSTT() + "," + stu.getMSSV() + "," + stu.getHoTen() + "," + stu.getGioiTinh() + "," + stu.getCMND());
		    		break;
		    	}
			}
		    csvReader.close();
		    
		    //Ghi lai xuong file cu
		    FileWriter csvWriter = new FileWriter(sche.getLop() + "-" + mamon + "-DSSV.csv");
			csvWriter.append("STT");
			csvWriter.append(",");
			csvWriter.append("MSSV");
			csvWriter.append(",");
			csvWriter.append("HoTen");
			csvWriter.append(",");
			csvWriter.append("GioiTinh");
			csvWriter.append(",");
			csvWriter.append("CMND");
			csvWriter.append("\n");
			
			for (String temp : allData) {
				String[] parts = temp.split(",");
			    csvWriter.append(parts[0] + "," + parts[1] + "," + parts[2] + "," + parts[3] + "," + parts[4] + "\n");
			}

			csvWriter.flush();
			csvWriter.close();
			
			//Xoa sinh vien trong bang diem
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
}
