package dao;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import pojo.Schedule;
import java.io.*;
import java.sql.*;
import dao.StudentDAO;
import pojo.Student;
import dao.GradeDAO;
import pojo.Grade;

public class GradeDAO {
    SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
	
	public void importGrade(String mm) {

		String jdbcURL = "jdbc:mysql://localhost:3306/mydb";
        String username = "root";
        String password = "lmfao#123";
        ScheduleDAO sch = new ScheduleDAO();
        Schedule s = sch.findById(mm);
        
        String csvFilePath = s.getLop() + "-" + mm + "-Diem.csv";
 
        int batchSize = 20;
 
        Connection connection = null;
 
        try {
 
            connection = DriverManager.getConnection(jdbcURL, username, password);
            connection.setAutoCommit(false);
 
            String sql = "INSERT INTO Diem (STT, MSSV, HoTen, DiemGK, DiemCK, DiemKhac, DiemTong, MaMon) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement statement = connection.prepareStatement(sql);
            BufferedReader lineReader = new BufferedReader(new FileReader(csvFilePath));
            String lineText = null;
 
            int count = 0;
 
            lineReader.readLine(); // skip header line
 
            while ((lineText = lineReader.readLine()) != null) {
                String[] data = lineText.split(",");
                String sTT = data[0];
                String mSSV = data[1];
                String hoTen = data[2];
                String diemGK = data[3];
                String diemCK = data[4];
                String diemKhac = data[5];
                String diemTong = data[6];
                String maMon = mm;
                
                int i = Integer.parseInt(sTT);
                statement.setInt(1, i);
                statement.setString(2, mSSV);
                statement.setString(3, hoTen);
                statement.setFloat(4, Float.parseFloat(diemGK));
                statement.setFloat(5, Float.parseFloat(diemCK));
                statement.setFloat(6, Float.parseFloat(diemKhac));
                statement.setFloat(7, Float.parseFloat(diemTong));
                statement.setString(8, mm);
 
                statement.addBatch();
 
                if (count % batchSize == 0) {
                    statement.executeBatch();
                }
            }
 
            lineReader.close();
 
            // execute the remaining queries
            statement.executeBatch();
 
            connection.commit();
            connection.close();
 
        } catch (IOException ex) {
            System.err.println(ex);
        } catch (SQLException ex) {
            ex.printStackTrace();
 
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
	}
	
	public List<Grade> subjectGrade(String sub){
		Session session = sessionFactory.openSession();
	    List<Grade> list = session.createQuery("FROM Grade").list();
	    return list;
	}
	
	public List<Grade> passedStudents(String sub){
		Session session = sessionFactory.openSession();
	    List<Grade> list = session.createQuery("FROM Grade WHERE DiemTong >= 5.0").list();
	    return list;
	}
	
	public float passedRate(String sub) {
		Session session = sessionFactory.openSession();
	    List<Grade> list1 = session.createQuery("FROM Grade WHERE DiemTong >= 5.0").list();
	    List<Grade> list2 = session.createQuery("FROM Grade").list();
	    float i = (float)(list1.size())/(list2.size());
	    return i;
	}
	
	public void update(Grade g) {
	    Session session = sessionFactory.openSession();
	    session.update(g);
	}
	
	public List<Grade> findById(String id) {
		 Session session = sessionFactory.openSession();
		 List<Grade> list = session.createQuery("FROM Grade WHERE MSSV LIKE :MSSV").setParameter("MSSV", "%" + id + "%").list();
		 return list;
	}
}
