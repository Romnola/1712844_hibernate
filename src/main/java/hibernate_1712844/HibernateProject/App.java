package hibernate_1712844.HibernateProject;

import java.util.List;
import dao.StudentDAO;
import dao.ScheduleDAO;
import pojo.Student;
import pojo.Schedule;
import dao.GradeDAO;
import pojo.Grade;
import dao.AccountDAO;
import pojo.Account;

public class App 
{
    public static void main( String[] args )
    {
    	//ScheduleDAO s = new ScheduleDAO();
    	//s.delete("1842001", "CTT001");
    	//showClassSchedule("18HCB");
    	//s.subjectStudentList();
    	//GradeDAO g =new GradeDAO();
    	//StudentShowGrade("1742005");
    	AccountDAO a = new AccountDAO();
    	Account acc = new Account();
    	acc.setTenTaiKhoan("1842001");
    	acc.setMatKhau("1234567");
    	acc.setLoai(1);
    	a.changePassword1(acc);
    	//ShowPassedStudents("CTT001");
    	//for (Schedule sche : a) {
	    //  System.out.println(sche.getMaMon());
	    //}
    }
    
    public static List<Student> ShowAllStudent() {
    	StudentDAO stuDAO = new StudentDAO();
    	List<Student> a = stuDAO.allStudent(); 	
    	return a;
    }
    
    public static void ShowSubjectGrade(String sub){
    	GradeDAO graDAO = new GradeDAO();
    	List<Grade> gra = graDAO.subjectGrade(sub);
    	for (Grade g : gra) {
	      System.out.println(g.getSTT() + ", " +g.getMSSV() + ", " +g.getDiemTong());
	    }
    }
    
    public static void ShowPassedStudents(String sub){
    	GradeDAO graDAO = new GradeDAO();
    	List<Grade> gra = graDAO.passedStudents(sub);
    	for (Grade g : gra) {
	      System.out.println(g.getSTT() + ", " +g.getMSSV() + ", " +g.getDiemTong());
	    }
    }
    
    public static void StudentShowGrade(String id) {
    	GradeDAO graDAO = new GradeDAO();
    	List<Grade> gra = graDAO.findById(id);
    	for (Grade g : gra) {
	      System.out.println(g.getMSSV() + ", " +g.getDiemTong());
	    }
    }
    
    public static List<Student> ShowClassStudent(String a) {
    	StudentDAO stuDAO = new StudentDAO();
    	List<Student> b = stuDAO.allClassStudent(a);
    	//for (Student student : b) {
	    //  System.out.println(student.getMSSV());
	    //}
    	return b;
    }
    
    public static void AddStudent(Student s) {
    	StudentDAO stuDAO = new StudentDAO();
    	List<Student> temp = ShowClassStudent(s.getLop());
    	s.setSTT(temp.size()+1);
    	stuDAO.add(s);
    }
    
    public static void AddStudent() {
    	Student newstu = new Student();
    	StudentDAO addStu =  new StudentDAO();
    	addStu.add(newstu);
    }
    
    public static void showSubjectStudent(String s) {
    	ScheduleDAO sch = new ScheduleDAO();
    	List<String> a = sch.allSubjectStudent(s);
    	for (String str : a) {
	      System.out.println(str);
	    }
    }
    
    public static void showClassSchedule(String c) {
    	ScheduleDAO sch = new ScheduleDAO();
    	List<Schedule> a = sch.classSchedule(c);
    	for (Schedule str : a) {
	      System.out.println(str);
	    }
    }
}
