package pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "diem")
public class Grade implements Serializable {
	@Id
	@Column(name = "MSSV")
	private String MSSV;
	
	@Id
	@Column(name = "MaMon")
	private String MaMon;
	
	@Column(name = "HoTen")
	private String HoTen;
	
	@Column(name = "STT")
	private int STT;
	
	@Column(name = "DiemGK")
	private Float DiemGK;
	
	@Column(name = "DiemCK")
	private Float DiemCK;
	
	@Column(name = "DiemKhac")
	private Float DiemKhac;
	
	@Column(name = "DiemTong")
	private Float DiemTong;
	
	
	public Grade() {
		
	}

	public Grade(String mSSV, String maMon,String hoTen, int sTT, float diemGK, float diemCK, float diemKhac, float diemTong) {
		this.MSSV = mSSV;
		this.MaMon = maMon;
		this.HoTen = hoTen;
		this.STT = sTT;
		this.DiemGK = diemGK;
		this.DiemCK = diemCK;
		this.DiemKhac = diemKhac;
		this.DiemTong = diemTong;
	}

	public String getMSSV() {
		return MSSV;
	}

	public void setMSSV(String mSSV) {
		this.MSSV = mSSV;
	}

	public String getMaMon() {
		return MaMon;
	}

	public void setMaMon(String maMon) {
		this.MaMon = maMon;
	}

	public int getSTT() {
		return STT;
	}

	public void setSTT(int sTT) {
		this.STT = sTT;
	}

	public float getDiemGK() {
		return DiemGK;
	}

	public void setDiemGK(float diemGK) {
		this.DiemGK = diemGK;
	}

	public float getDiemCK() {
		return DiemCK;
	}

	public void setDiemCK(float diemCK) {
		this.DiemCK = diemCK;
	}

	public float getDiemKhac() {
		return DiemKhac;
	}

	public void setDiemKhac(float diemKhac) {
		this.DiemKhac = diemKhac;
	}

	public float getDiemTong() {
		return DiemTong;
	}

	public void setDiemTong(float diemTong) {
		this.DiemTong = diemTong;
	}
	
	
}
