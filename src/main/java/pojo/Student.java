package pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "sinhvien")
public class Student {
	@Id
	@Column(name = "MSSV")
	private String MSSV;
	
	@Column(name = "STT")
	private int STT;
	
	@Column(name = "HoTen")
	private String HoTen;
	
	@Column(name = "GioiTinh")
	private String GioiTinh;
	
	@Column(name = "CMND")
	private String CMND;
	
	@Column(name = "Lop")
	private String Lop;
	
	public Student() {
		
	}
	
	public Student(int STT, String MSSV, String HoTen, String GioiTinh, String CMND, String Lop) {
		this.STT = STT;
		this.MSSV = MSSV;
		this.HoTen = HoTen;
		this.GioiTinh = GioiTinh;
		this.CMND = CMND;
		this.Lop = Lop;
	}

	public String getMSSV() {
		return MSSV;
	}

	public void setMSSV(String mSSV) {
		this.MSSV = mSSV;
	}

	public int getSTT() {
		return STT;
	}

	public void setSTT(int sTT) {
		this.STT = sTT;
	}

	public String getHoTen() {
		return HoTen;
	}

	public void setHoTen(String hoTen) {
		this.HoTen = hoTen;
	}

	public String getGioiTinh() {
		return GioiTinh;
	}

	public void setGioiTinh(String gioiTinh) {
		this.GioiTinh = gioiTinh;
	}

	public String getCMND() {
		return CMND;
	}

	public void setCMND(String cMND) {
		this.CMND = cMND;
	}

	public String getLop() {
		return Lop;
	}

	public void setLop(String lop) {
		this.Lop = lop;
	}
	
}
