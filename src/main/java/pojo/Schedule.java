package pojo;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "thoikhoabieu")
public class Schedule {
	@Id
	@Column(name = "MaMon")
	private String MaMon;
	
	@Column(name = "STT")
	private int STT;
	
	@Column(name = "TenMon")
	private String TenMon;
	
	@Column(name = "PhongHoc")
	private String PhongHoc;
	
	@Column(name = "Lop")
	private String Lop;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	  @JoinTable(name = "diem", 
	    joinColumns = { @JoinColumn(name = "MaMon") }, 
	    inverseJoinColumns = {@JoinColumn(name = "MSSV") })
	  private Set<Student> schedules = new HashSet<Student>();
	
	public Schedule() {
		
	}
	
	public Schedule(int STT, String MaMon, String TenMon, String PhongHoc, String Lop) {
		this.STT=STT;
		this.MaMon=MaMon;
		this.TenMon=TenMon;
		this.PhongHoc=PhongHoc;
		this.Lop=Lop;
	}

	public String getMaMon() {
		return MaMon;
	}

	public void setMaMon(String maMon) {
		MaMon = maMon;
	}

	public int getSTT() {
		return STT;
	}

	public void setSTT(int sTT) {
		this.STT = sTT;
	}

	public String getTenMon() {
		return TenMon;
	}

	public void setTenMon(String tenMon) {
		this.TenMon = tenMon;
	}

	public String getPhongHoc() {
		return PhongHoc;
	}

	public void setPhongHoc(String phongHoc) {
		this.PhongHoc = phongHoc;
	}

	public String getLop() {
		return Lop;
	}

	public void setLop(String lop) {
		this.Lop = lop;
	}
	
	
}
