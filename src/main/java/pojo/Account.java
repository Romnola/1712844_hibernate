package pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "taikhoan")
public class Account {
	@Id
	@Column(name = "TenTaiKhoan")
	private String TenTaiKhoan;
	
	@Column(name = "MatKhau")
	private String MatKhau;
	
	@Column(name = "Loai")
	private int Loai;
	
	public Account() {
		
	}

	public Account(String tentaikhoan, String password, int loai) {
		this.TenTaiKhoan = tentaikhoan;
		this.MatKhau = password;
		this.Loai = loai;
	}

	public String getTenTaiKhoan() {
		return TenTaiKhoan;
	}

	public void setTenTaiKhoan(String tenTaiKhoan) {
		this.TenTaiKhoan = tenTaiKhoan;
	}

	public String getMatKhau() {
		return MatKhau;
	}

	public void setMatKhau(String matKhau) {
		this.MatKhau = matKhau;
	}

	public int getLoai() {
		return Loai;
	}

	public void setLoai(int loai) {
		this.Loai = loai;
	}
	
	
	
}
